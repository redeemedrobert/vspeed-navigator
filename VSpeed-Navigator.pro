CONFIG -= qt

TEMPLATE = lib
DEFINES += VSPEEDNAVIGATOR_LIBRARY \
            IBM=1

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    data.cpp \
    dllmain.cpp \
    vnav.cpp

HEADERS += \
    VSpeed-Navigator_global.h \
    XPLMDataAccess.h \
    XPLMDefs.h \
    XPLMMenus.h \
    XPLMNavigation.h \
    XPLMProcessing.h \
    XPLMUtilities.h \
    XPWidgets.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

LIBS += \
    $$PWD/XPLM_64.lib
