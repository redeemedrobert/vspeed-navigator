#include <stdio.h>

#include "XPLMProcessing.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"

extern float getTargetAlt();
extern float getAlt();
extern float getNextFmsDistance();
extern float getGroundSpeed();
extern bool getDataRefs();

extern void setVSpeed(float speed);

float doVnav(float, float, int, void*);

float continueNav = 0.0;

void startVnav()
{
    continueNav = 0.5;
    XPLMRegisterFlightLoopCallback(doVnav, continueNav, NULL);
    XPLMDebugString("VSpeed Navigator: Starting VNAV\n");
}

void stopVnav()
{
    continueNav = 0.0;
    XPLMUnregisterFlightLoopCallback(doVnav, NULL);
    XPLMDebugString("VSpeed Navigator: Stopping VNAV\n");
}

float getRequiredVSpeed()
{
    if(getTargetAlt() <= 0) return 0; // don't want to descend just because there is no target altitude set for a waypoint
    float eta_minutes = getNextFmsDistance() / (getGroundSpeed() * 60); // getGroundSpeed returns meters/second, needs to be meters/minute
    char debug[512];
    snprintf(debug, sizeof debug, "VSpeed Navigator: getRequiredVSpeed\ngetGroundSpeed - %f \ngetNextFmsDistance - %f \neta_minutes - %f \ngetTargetAlt - %f \ngetAlt - %f \n", getGroundSpeed(), getNextFmsDistance(), eta_minutes, getTargetAlt(), getAlt());
    XPLMDebugString(debug);
    return (getTargetAlt() - getAlt()) / eta_minutes;
}

float doVnav(float, float, int, void*)
{
    XPLMDebugString("VSpeed Navigator: doVnav()\n");
    float vspeed = getRequiredVSpeed();
    char debug[64];
    snprintf(debug, sizeof debug, "VSpeed Navigator: getRequiredVSpeed - %f \n", vspeed);
    XPLMDebugString(debug);
    if(vspeed > 5000 || vspeed < -5000) // something's gone wrong with the maths or the flight plan is too extreme
        setVSpeed(0.0);
    else
        setVSpeed(vspeed);
    return continueNav; // returns interval timer until stopVnav is called, at which point it returns 0 to stop vnav
}
