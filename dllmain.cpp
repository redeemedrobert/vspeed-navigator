#include "XPLMDefs.h"
#include "XPLMMenus.h"

int g_menu_in = 0;
int g_visible = 0;

XPLMMenuID g_menu_id = 0;

void menu_hand(void* inMenu, void* inItem);

extern bool getDataRefs();
extern void startVnav();
extern void stopVnav();


PLUGIN_API int XPluginStart(char* outName, char* outSig, char* outDesc)
{
    strcpy(outName, "VSpeed Navigator");
    strcpy(outSig, "dev.robertaugustine.vspeed-navigator");
    strcpy(outDesc, "A plugin to assist with the calculation of vertical speed and navigation of vertical paths in planes without VNAV.");

    g_menu_in = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "VSpeed Navigator", 0, 0);
    g_menu_id = XPLMCreateMenu("VSpeed Navigator", XPLMFindPluginsMenu(), g_menu_in, menu_hand, NULL);
    XPLMAppendMenuItem(g_menu_id, "Begin FMS VNAV", (void*)"vnav_start", 1);
    XPLMAppendMenuItem(g_menu_id, "Stop FMS VNAV", (void*)"vnav_stop", 1);


    return getDataRefs(); // returns whether or not all required data refs are valid
}

PLUGIN_API void	XPluginStop(void)
{
}

PLUGIN_API void XPluginDisable(void)
{
}

PLUGIN_API int  XPluginEnable(void)
{
    return 1;
}

PLUGIN_API void XPluginReceiveMessage(XPLMPluginID, int, void*)
{
}

void menu_hand(void*, void* item)
{
    if(strcmp((const char*)item, "vnav_start") == 0)
    {
        startVnav();
    }
    else if(strcmp((const char*)item, "vnav_stop") == 0)
    {
        stopVnav();
    }
}
