#include <tgmath.h>
#include <stdio.h>
#include <string.h>

#include "XPLMDataAccess.h"
#include "XPLMNavigation.h"
#include "XPLMUtilities.h"

XPLMDataRef dialedAlt;
XPLMDataRef altitude;
XPLMDataRef targetAlt;
XPLMDataRef vspeed;
XPLMDataRef gspeed;
XPLMDataRef latitude;
XPLMDataRef longitude;

float getPlaneLat();
float getPlaneLon();

bool getDataRefs()
{
    // XPLMFindDataRef();
    dialedAlt = XPLMFindDataRef("sim/cockpit/autopilot/altitude");
    targetAlt = XPLMFindDataRef("sim/cockpit2/radios/indicators/fms_fpta_pilot");
    vspeed = XPLMFindDataRef("sim/cockpit/autopilot/vertical_velocity");
    gspeed = XPLMFindDataRef("sim/flightmodel/position/groundspeed");
    latitude = XPLMFindDataRef("sim/flightmodel/position/latitude");
    longitude = XPLMFindDataRef("sim/flightmodel/position/longitude");
    altitude = XPLMFindDataRef("sim/flightmodel/position/elevation");

    return true;
    // it seems these always return != 1 at startup, so skip it...
//    return (
//        XPLMIsDataRefGood(dialedAlt) == 1 &&
//        XPLMIsDataRefGood(targetAlt) == 1 &&
//        XPLMIsDataRefGood(vspeed) == 1 &&
//        XPLMIsDataRefGood(gspeed) == 1 &&
//        XPLMIsDataRefGood(latitude) == 1 &&
//        XPLMIsDataRefGood(longitude) == 1 &&
//        XPLMIsDataRefGood(altitude) == 1
//    ); // this return is returned used by XPluginStart. The validity of these refs will determine successful plugin start
}

float getNextFmsAltitude()
{
    int dest = XPLMGetDestinationFMSEntry();
    int alt = 0;
    XPLMGetFMSEntryInfo(dest, NULL, NULL, NULL, &alt, NULL, NULL);
    return alt;
}

int getNextFmsDistance()
{
    int dest = XPLMGetDestinationFMSEntry();
    float lat;
    float lon;
    float latDist;
    float lonDist;
    float clen;
    float dist;
    XPLMGetFMSEntryInfo(dest, NULL, NULL, NULL, NULL, &lat, &lon);
    latDist = lat-getPlaneLat();
    lonDist = lon-getPlaneLon();
    if(latDist < 0) latDist *= -1; // I don't want to deal in negatives
    if(lonDist < 0) lonDist *= -1;
    clen = sqrt(pow(latDist, 2) + pow(lonDist, 2)); // a^2 + b^2 = c^2, thanks Pythagoras
    dist = clen * 111 * 1000; // 1 degree is 111km, we want meters
    char debug[512];
    snprintf(debug, sizeof debug, "VSpeed Navigator: getNextFmsDistance\nlat - %f \nlon - %f \nlatDist - %f \n lonDist - %f \nclen - %f \n dist %f \n", lat, lon, latDist, lonDist, clen, dist);
    XPLMDebugString(debug);
    memset(debug, 0, sizeof debug);
    snprintf(debug, sizeof debug, "getLat - %f\ngetLon - %f\n", getPlaneLat(), getPlaneLon());
    XPLMDebugString(debug);
    return dist;
}

float getPlaneLat()
{
    return XPLMGetDataf(latitude);
}

float getPlaneLon()
{
    return XPLMGetDataf(longitude);
}

float getTargetAlt()
{
    return getNextFmsAltitude();
}

float getAlt()
{
    return XPLMGetDataf(altitude) * 3.281; // dataref returns meters, we need feet
}

float getGroundSpeed()
{
    return XPLMGetDataf(gspeed);
}

void setVSpeed(float speed)
{
    XPLMSetDataf(vspeed, speed);
}
